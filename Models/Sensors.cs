using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysCribBackend.Models
{
    public class Sensors
    {
        #region Properties
            
        public int Id { get; set; }
        public int Crib_id { get; set; }
        public enum SensorType { humidity, motion, sound }
        public SensorType Sensor { get; set; }

        public string model { get; set; } = string.Empty;

        public enum SensorStatus { on, off }

        public SensorStatus status { get; set; }
    
        #endregion
    }
}