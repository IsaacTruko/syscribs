using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysCribBackend.Models
{
    public class Suscriptions
    {
        #region Properties
            
        public int Id { get; set; }
        public int user_id { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public enum Plan {  Month, Year }
        public Plan plan { get; set; }

        #endregion
    }
}