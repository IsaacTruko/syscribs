using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysCribBackend.Models
{
    public class Users
    {
        #region Properties
        public int Id { get; set; }
        public string UserName { get; set; } = string.Empty;
        public string FirstName { get; set; } = string.Empty;
        public string LastName { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public bool Suscribed { get; set; } = false;

        public enum Type { Parent, Caregiver, Admin }

        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public bool active { get; set; } = true;
            
        #endregion
    }
}